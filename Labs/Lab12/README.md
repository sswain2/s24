# Lab 12

1. Start work on `UserPredictor` of P6

2. Use a LinearRegression to [predict how long](./regression) it will take to run a piece of code

3. Practice [comparing models](./model-comparison)

4. Review [dot products/matrix multiplication](./dot-product-matrix-multiplication)

5. Try to fix the bugs in the [`main.ipynb`](./8-geo/main.ipynb).  There is a `solution.ipynb` too, but only peek as a last resort!

# Screenshot Requirement

Submit your process of `UserPredictor` 

To provide you with flexibility (or peace of mind), we have set the P5 deadline for April 23. However, we strongly recommend that you complete your project before this date and begin working on P6. As an incentive to start P6 early, we may assign a task in lab 12 that encourages you to finish P5 ahead of schedule and start working on P6.